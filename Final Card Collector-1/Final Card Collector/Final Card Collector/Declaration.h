#pragma once
#include <vector>
#include <string>
#include <fstream>
using namespace std;


//Function Declarations
char mainMenu();

void Intro();

void addCard(vector<string> &cardList);

void display(vector<string> &cardList);

void removeCard(vector<string> &bookList);

void exportCards(vector<string> &cardList, fstream &cardFile);




//The base call al other Inherit from.
class Card
{
public:
	Card();
	Card(string cardType, string cardName);

	string getType();
	void setType(string cardType);
	string getName();
	void setName(string cardname);

protected:
	string type;
	string name;
};

class Pokemon_Card : public Card
{
public:
	//The many constructors for all card possibilities.
	Pokemon_Card();
	Pokemon_Card(string pType, string pName, string pAbility, string fAttack, string sAttack, string tAttack);
	Pokemon_Card(string pType, string pName, string pAbility, string fAttack, string sAttack);
	Pokemon_Card(string pType, string pName, string pAbility, string fAttack);
	Pokemon_Card(string pType, string pName, string fAttack);
	string getAbility();
	void setAbility(string pAbility);
	string getAttack1();
	void setAttack1(string fAttack);
	string getAttack2();
	void setAttack2(string sAttack);
	string getAttack3();
	void setAttack3(string tAttack);

private:
	string ability;
	string attack1;
	string attack2;
	string attack3;

};
//Trainer card class.
class Trainer_Card : public Card
{
public:
	//The many constructors for all card possibilities.
	Trainer_Card();
	Trainer_Card(string tType, string tName, string fEffect, string sEffect);
	Trainer_Card(string tType, string tName, string fEffect);
	string getEffect1();
	void setEffect1(string fEffect);
	string getEffect2();
	void setEffect2(string sEffect);

private:
	string effect1;
	string effect2;
};
//Class for basic/normal engery as well as parent class
class Energy_Card : public Card
{
public:
	Energy_Card();
	Energy_Card(string eType, string eName, string eColor);
	string getColor();
	void setColor(string eColor);
	//Curiosity made me check if I could do this.
protected:
	string color;
};
//Class for special engery 
class Special_Energy : public Energy_Card
{
public:
	Special_Energy();
	Special_Energy(string seType, string seName, string seColor, string seEffect);
	string getEffect();
	void setEffect(string seEffect);
private:
	string effect;
};