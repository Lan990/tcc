#include "Declaration.h"
#include <iostream>
#include<string>
#include <vector>
#include <fstream>
#include<algorithm>


using namespace std;
//The intro to the program.
void Intro()
{
	cout << "Card Master Card Collector by M.Shepherd CSC275\n"
		<< "\n\nHello and welcome to the Card Master Card Collector."
		<< "\n With this program you will be able to document all the cards you own and actively edit that list, as well as export it."
		<< "\n (Currently this program is formatted for Pokemon cards but support for other card types such as Yu-Gi-Oh! is planned.)" << endl;

}
//The hub/ main menu.
char mainMenu()
{
	char choice = 'f';
	do
	{//Do try and catches insure the program doesn't crash.
		try
		{
			cout << "\nWhat would you like to do?\n"
				<< "1-Add card to collection\n"
				<< "2-Remove card from collection\n"
				<< "3-View collection\n"
				<< "4-Export collection and exit Program" << endl;


			cin >> choice;
			cin.ignore();
			if (choice != '1' && choice != '2' && choice != '3' && choice != '4'&& choice != '5' && choice != '6')
			{
				throw "Catch type char* \nThat was not an option.";
			}
		}
		catch (char * message)
		{
			cout << message << endl;
		}
		catch (...)
		{
			cout << "Error: What you entered was not a proper response." << endl;
		}
	} while (choice != '1' && choice != '2' && choice != '3' && choice != '4'&& choice != '5' && choice != '6');
	return choice;
}


void removeCard(vector<string> &cardList)
{//x is the number of what element the player want's to remove
	int x;
	string remove;
	display(cardList);
	cout << "Which card would you like to remove?" << endl;
	cin >> x;
	// x-1 because while the displyed list starts from one vectors don't.
	x--;
	//makes remove string equal to the element the user want's to remove so the Iterator can go through and remove it without the user needing to enter the full string.
	remove = cardList[x];
	vector<string>::iterator cardIter;
	cardIter = find(cardList.begin(), cardList.end(), remove);
	if (cardIter != cardList.end())
	{
		cardList.erase(cardIter);
	}

	else
	{
		cout << "You messed up" << endl;
	}
	cout << "\nThis is the collection now." << endl;
	display(cardList);
}


// goes through the entire vector with an Iterator and writes the strings to the card collection file.
void exportCards(vector<string> &cardList, fstream &cardFile)
{
	cardFile.open("cardCollection.txt");
	sort(cardList.begin(), cardList.end());
	vector<string>::iterator Iter;
	for (Iter = cardList.begin(); Iter != cardList.end(); Iter++)
	{
		cardFile << *Iter << endl;
	}
	cardFile.close();
	cout << "Collection exported." << endl;
}



//The add card function is basicly three function in one that create one of the four card types.
void addCard(vector<string> &cardList)
{
	string cardinfo;
	string typeEnter;
	string nameEnter;
	string abilityEnter;
	//Renamed to holders for reuse.
	string holderEnter1;//rather then making veriable for attacks and effect the term holder works all around.
	string holderEnter2;
	string holderEnter3;
	char check;
	char selection = 'f';

	do
	{
		try
		{
			cout << "\nWhat type of card would you like to add?\n1-Pokemon\n2-Trainer\n3-Energy" << endl;
			cin >> selection;
			cin.ignore();

			if (selection != '1' && selection != '2'&& selection != '3')
			{
				throw "Catch type char* \nWhat you entered was not an option.";
			}
		}
		catch (char * message)
		{
			cout << message << endl;
		}
		catch (...)
		{
			cout << "Error: What you entered was not a proper response." << endl;
		}
	} while (selection != '1' && selection != '2'&& selection != '3');
	if (selection == '1')// if the user wants to add a pokemon card.
	{
		typeEnter = "Pokemon Card";
		cout << "\nEnter the Pokemon's name." << endl;
		getline(cin, nameEnter);
		do
		{
			try
			{


				cout << "Does " << nameEnter << " have an ability?\n Enter y-Yes or n-No" << endl;//Where the first split of the pokemon card builder starts
				cin >> check;
				cin.ignore();

				if (check != 'y' && check != 'Y' && check != 'n' && check != 'N')
				{
					throw "Catch type char* \nThat was not a proper response please enter y for Yes or n for No";
				}
			}
			catch (char* message)
			{
				cout << message << endl;
			}
		} while (check != 'y' && check != 'Y' && check != 'n' && check != 'N');
		//Ability Block
		if (check == 'y' || check == 'Y')
		{
			cout << "Please enter the ability." << endl;
			getline(cin, abilityEnter);
			cout << "\nWhat is this Pokemon's first attack?" << endl;
			getline(cin, holderEnter1);
			do
			{
				try
				{
					cout << "Does this Pokemon have another attack?\nEnter y-Yes or n-No." << endl;
					cin >> check;
					cin.ignore();
					if (check != 'y' && check != 'Y' && check != 'n' && check != 'N')
					{
						throw "Catch type char* \nThat was not a proper response please enter y for Yes or n for No";
					}
				}
				catch (char* message)
				{
					cout << message << endl;
				}
			} while (check != 'y' && check != 'Y' && check != 'n' && check != 'N');

			if (check == 'y' || check == 'Y')
			{
				cout << "\nWhat is this Pokemon's next attack?" << endl;
				getline(cin, holderEnter2);

				do
				{
					try
					{

						cout << "Does this Pokemon have another attack?\nEnter y-Yes or n-No" << endl;
						cin >> check;
						cin.ignore();

						if (check != 'y' && check != 'Y' && check != 'n' && check != 'N')
						{
							throw "Catch type char* \nThat was not a proper response please enter y for Yes or n for No";
						}
					}
					catch (char* message)
					{
						cout << message << endl;
					}
				} while (check != 'y' && check != 'Y' && check != 'n' && check != 'N');

				if (check == 'y' || check == 'Y')
				{
					cout << "\nWhat is this Pokemon's last attack?" << endl;
					getline(cin, holderEnter3);
					Pokemon_Card(typeEnter, nameEnter, abilityEnter, holderEnter1, holderEnter2, holderEnter3);
					//Appends the information to a string in a organized structure. 
					cardinfo.append(typeEnter);
					cardinfo.append("\n");
					cardinfo.append(abilityEnter);
					cardinfo.append(" ");
					cardinfo.append(nameEnter);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter1);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter2);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter3);

				}
				if (check == 'n' || check == 'N')
				{
					Pokemon_Card(typeEnter, nameEnter, abilityEnter, holderEnter1, holderEnter2);
					cardinfo.append(typeEnter);
					cardinfo.append("\n");
					cardinfo.append(abilityEnter);
					cardinfo.append(" ");
					cardinfo.append(nameEnter);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter1);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter2);
				}


			}
			if (check == 'n' || check == 'N')
			{
				Pokemon_Card(typeEnter, nameEnter, abilityEnter, holderEnter1);
				cardinfo.append(typeEnter);
				cardinfo.append("\n");
				cardinfo.append(abilityEnter);
				cardinfo.append(" ");
				cardinfo.append(nameEnter);
				cardinfo.append("\n");
				cardinfo.append("-");
				cardinfo.append(holderEnter1);
			}


		}//No Ability block
		if (check == 'n' || check == 'N')
		{
			cout << "\nWhat is this Pokemon's first attack?" << endl;
			getline(cin, holderEnter1);
			do
			{
				try
				{
					cout << "Does this Pokemon have another attack?\nEnter y-Yes or n-No." << endl;
					cin >> check;
					cin.ignore();
					if (check != 'y' && check != 'Y' && check != 'n' && check != 'N')
					{
						throw "Catch type char* \nThat was not a proper response please enter y for Yes or n for No";
					}
				}
				catch (char* message)
				{
					cout << message << endl;
				}
			} while (check != 'y' && check != 'Y' && check != 'n' && check != 'N');

			if (check == 'y' || check == 'Y')
			{
				cout << "\nWhat is this Pokemon's next attack?" << endl;
				getline(cin, holderEnter2);
				do
				{
					try
					{
						cout << "Does this Pokemon have another attack?" << endl;
						cin >> check;
						cin.ignore();
						if (check != 'y' && check != 'Y' && check != 'n' && check != 'N')
						{
							throw "Catch type char* \nThat was not a proper response please enter y for Yes or n for No";
						}
					}
					catch (char* message)
					{
						cout << message << endl;
					}
				} while (check != 'y' && check != 'Y' && check != 'n' && check != 'N');

				if (check == 'y' || check == 'Y')
				{
					cout << "\nWhat is this Pokemon's last attack?" << endl;
					getline(cin, holderEnter3);
					Pokemon_Card(typeEnter, nameEnter, holderEnter1, holderEnter2, holderEnter3);
					cardinfo.append(typeEnter);
					cardinfo.append("\n");
					cardinfo.append(nameEnter);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter1);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter2);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter3);
				}

				if (check == 'n' || check == 'N')
				{
					Pokemon_Card(typeEnter, nameEnter, holderEnter1, holderEnter2);
					cardinfo.append(typeEnter);
					cardinfo.append("\n");
					cardinfo.append(nameEnter);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter1);
					cardinfo.append("\n");
					cardinfo.append("-");
					cardinfo.append(holderEnter2);
				}
			}
			if (check == 'n' || check == 'N')
			{
				Pokemon_Card(typeEnter, nameEnter, holderEnter1);
				cardinfo.append(typeEnter);
				cardinfo.append("\n");
				cardinfo.append(nameEnter);
				cardinfo.append("\n");
				cardinfo.append("-");
				cardinfo.append(holderEnter1);


			}
		}
	}
	if (selection == '2')
	{//The block to make trainer cards
		typeEnter = "Trainer Card";
		cout << "\nWhat is the Trainer card called?" << endl;
		getline(cin, nameEnter);
		cout << "What does the card do?" << endl;
		getline(cin, holderEnter1);
		do
		{
			try
			{
				cout << "\nDoes this card have another effect?\nEnter y-Yes or n-No" << endl;//The only split in this one is if it has a secound effect.
				cin >> check;
				cin.ignore();
				if (check != 'y' && check != 'Y' && check != 'n' && check != 'N')
				{
					throw "Catch type char* \nThat was not a proper response please enter y for Yes or n for No";
				}
			}
			catch (char* message)
			{
				cout << message << endl;
			}
		} while (check != 'y' && check != 'Y' && check != 'n' && check != 'N');

		if (check == 'y' || check == 'Y')
		{
			cout << "What is the secondary effect?" << endl;
			getline(cin, holderEnter2);
			//Appends the information to a string in a organized structure. 
			Trainer_Card(typeEnter, nameEnter, holderEnter1, holderEnter2);
			cardinfo.append(typeEnter);
			cardinfo.append("\n");
			cardinfo.append(nameEnter);
			cardinfo.append("\n");
			cardinfo.append(holderEnter1);
			cardinfo.append("\n");
			cardinfo.append("\n");
			cardinfo.append(holderEnter2);
		}
		if (check == 'n' || check == 'N')
		{
			Trainer_Card(typeEnter, nameEnter, holderEnter1);
			cardinfo.append(typeEnter);
			cardinfo.append("\n");
			cardinfo.append(nameEnter);
			cardinfo.append("\n");
			cardinfo.append(holderEnter1);
		}
	}
	if (selection == '3')
	{//The block to make energy cards
		do
		{
			try
			{
				cout << "\nIs your energy special (does it have an effect)?\nEnter y-Yes or n-No" << endl;
				cin >> check;
				cin.ignore();

				if (check != 'y' && check != 'Y' && check != 'n' && check != 'N')
				{
					throw "Catch type char* \nThat was not a proper response please enter y for Yes or n for No";
				}
			}
			catch (char* message)
			{
				cout << message << endl;
			}
		} while (check != 'y' && check != 'Y' && check != 'n' && check != 'N');

		if (check == 'y' || check == 'Y')//Block for special energy
		{
			typeEnter = "Special Energy Card";
			cout << "What is the name of this Special Energy?" << endl;
			getline(cin, nameEnter);
			cout << "What color is this energy?" << endl;
			getline(cin, holderEnter1);
			cout << "Now enter what it does." << endl;
			getline(cin, holderEnter2);
			Special_Energy(typeEnter, nameEnter, holderEnter1, holderEnter2);
			cardinfo.append(typeEnter);
			cardinfo.append("\n");
			cardinfo.append(nameEnter);
			cardinfo.append("\n");
			cardinfo.append(holderEnter1);
			cardinfo.append("\n");
			cardinfo.append("\n");
			cardinfo.append(holderEnter2);
		}
		if (check == 'n' || check == 'N')//Block for normal energy
		{
			typeEnter = "Energy Card";
			nameEnter = "Normal";
			cout << "What color is this energy?" << endl;
			getline(cin, holderEnter1);
			Energy_Card(typeEnter, nameEnter, holderEnter1);
			cardinfo.append(typeEnter);
			cardinfo.append("\n");
			cardinfo.append(nameEnter);
			cardinfo.append("\n");
			cardinfo.append(holderEnter1);
		}

	}
	//Takes the srting and adds it to the card list.
	cardList.push_back(cardinfo);
	cout << "Card added" << endl;


}

void display(vector<string> &cardList)// goes through the entire vector with an Iterator, gives it a number and cout's it.
{
	vector<string>::iterator iter;
	int num = 1;
	cout << "\nThis is the collection thus far.\n" << endl;
	for (iter = cardList.begin(); iter != cardList.end(); iter++)
	{
		cout << num << "- " << *iter << endl << endl;
		num++;
	}
	cout << "\n" << endl;
}






//class Definitions
Card::Card() {}

Card::Card(string cardType, string cardName)
{
	type = cardType;
	name = cardName;
}

string Card::getType()
{
	return type;
}

void Card::setType(string cardType)
{
	type = cardType;
}
string Card::getName()
{
	return name;
}
void Card::setName(string cardName)
{
	name = cardName;
}

Pokemon_Card::Pokemon_Card()
{}

Pokemon_Card::Pokemon_Card(string cardType, string cardName, string pAbility, string fAttack, string sAttack, string tAttack) : Card::Card(cardType, cardName)
{
	Card::Card(cardType, cardName);
	ability = pAbility;
	attack1 = fAttack;
	attack2 = sAttack;
	attack3 = tAttack;
}

Pokemon_Card::Pokemon_Card(string cardType, string cardName, string pAbility, string fAttack, string sAttack) : Card::Card(cardType, cardName)
{
	Card::Card(cardType, cardName);
	ability = pAbility;
	attack1 = fAttack;
	attack2 = sAttack;
}

Pokemon_Card::Pokemon_Card(string cardType, string cardName, string pAbility, string fAttack) : Card::Card(cardType, cardName)
{
	Card::Card(cardType, cardName);
	ability = pAbility;
	attack1 = fAttack;
}

Pokemon_Card::Pokemon_Card(string cardType, string cardName, string fAttack) : Card::Card(cardType, cardName)
{
	Card::Card(cardType, cardName);
	attack1 = fAttack;
}


string Pokemon_Card::getAbility()
{
	return ability;
}

void Pokemon_Card::setAbility(string pAbility)
{
	ability = pAbility;
}

string Pokemon_Card::getAttack1()
{
	return attack1;
}

void Pokemon_Card::setAttack1(string fAttack)
{
	attack1 = fAttack;
}

string Pokemon_Card::getAttack2()
{
	return attack2;
}

void Pokemon_Card::setAttack2(string sAttack)
{
	attack2 = sAttack;
}

string Pokemon_Card::getAttack3()
{
	return attack3;
}

void Pokemon_Card::setAttack3(string tAttack)
{
	attack3 = tAttack;
}

Trainer_Card::Trainer_Card()
{}

Trainer_Card::Trainer_Card(string cardType, string cardName, string fEffect, string sEffect) :Card::Card(cardType, cardName)
{
	effect1 = fEffect;
	effect2 = sEffect;
}

Trainer_Card::Trainer_Card(string cardType, string cardName, string fEffect) :Card::Card(cardType, cardName)
{
	effect1 = fEffect;

}

string Trainer_Card::getEffect1()
{
	return effect1;
}

void Trainer_Card::setEffect1(string fEffect)
{
	effect1 = fEffect;
}

string Trainer_Card::getEffect2()
{
	return effect2;
}

void Trainer_Card::setEffect2(string sEffect)
{
	effect2 = sEffect;
}

Energy_Card::Energy_Card()
{}

Energy_Card::Energy_Card(string cardType, string cardName, string eColor) : Card::Card(cardType, cardName)
{
	color = eColor;
}

string Energy_Card::getColor()
{
	return color;
}

void Energy_Card::setColor(string eColor)
{
	color = eColor;
}

Special_Energy::Special_Energy()
{}

Special_Energy::Special_Energy(string cardType, string cardName, string eColor, string seEffect) : Energy_Card::Energy_Card::Energy_Card(cardType, cardName, eColor)
{
	effect = seEffect;
}

string Special_Energy::getEffect()
{
	return effect;
}

void Special_Energy::setEffect(string seEffect)
{
	effect = seEffect;
}