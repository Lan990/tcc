#include "Declaration.h"
#include <iostream> 
#include <vector>   
#include<string>
#include <fstream>

using namespace std;


int main()
{
	vector<string> card;
	vector<string> ::iterator cardIter;
	fstream cardFile;
	char menu;
	char c;
	bool program = true;



	Intro();

	do
	{
		menu = mainMenu();
		if (menu == '1')
		{
			addCard(card);
		}
		if (menu == '2')
		{
			removeCard(card);
		}
		if (menu == '3')
		{
			display(card);
		}
		if (menu == '4')
		{
			exportCards(card, cardFile);
			do
			{
				try
				{

					cout << "\nWould you like to exit the program? Y-yes N-no" << endl;
					cin >> c;
					cin.ignore();
					if (c != 'y' && c != 'Y' && c != 'n' && c != 'N')
					{
						throw "Catch type char* \nThat was not a proper response please enter y for Yes or n for No";
					}
				}
				catch (char* message)
				{
					cout << message << endl;
				}
			} while (c != 'y' && c != 'Y' && c != 'n' && c != 'N');
			if (c == 'y' || c == 'Y')
			{
				program = false;
			}
			if (c == 'n' || c == 'N')
			{
				program = true;
			}
		}
	} while (program == true);

	cout << "\nThank you for using this program and I hope you have a grate rest of your day." << endl;
	system("pause");
	return 0;
}


